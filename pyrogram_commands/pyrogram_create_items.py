from pyrogram import Client, filters
from tortoise_models.basemodel import User


@Client.on_message(filters.command("create", "/") & filters.private)
async def pyrogram_create(client, message):
    if len(message.command) < 2:
        return await client.send_message(chat_id=message.chat.id, text="Необходимо указать имя.")
    user_name = message.command[1]
    if await User.filter(user_name=user_name).first() is not None:
        return await client.send_message(chat_id=message.chat.id, text="Такое имя уже используется.")
    else:
        await User.create(user_id=message.chat.id, user_name=user_name)
        return await client.send_message(chat_id=message.chat.id, text="Новое имя создано успешно.")


#                          Пример диалога с ботом
#   USER: /create
#                                             Необходимо указать имя. :BOT
#   USER: /create Ivan
#                                          Новое имя создано успешно. :BOT
#   USER: /create Ivan
#                                         Такое имя уже используется. :BOT